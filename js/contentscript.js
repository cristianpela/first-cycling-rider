//first we check if we're ready;
chrome.runtime.sendMessage({ action: "checkForReady" }, function (response) {
    if (response)
        onExtensionReady();
});

//listen to extension errors;
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.type === "error") {
            alert(request.message);
        }
    }
);

function onExtensionReady() {

    

    Handlebars.getTemplate = function (name) {
        if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
            $.ajax({
                url: chrome.extension.getURL('template/' + name + '.html'),
                success: function (data) {
                    if (Handlebars.templates === undefined) {
                        Handlebars.templates = {};
                    }
                    Handlebars.templates[name] = Handlebars.compile(data);
                },
                async: false
            });
        }
        return Handlebars.templates[name];
    };

    var url = window.location.href;
    var riderid = getRiderId(url);
    var raceid = getRaceId(url);
    var scoreDetailTpl = Handlebars.getTemplate('scoredetail');
    var cachedScores = {};

    if (riderid > -1) {
        var rider = $("#wrapper h1");
        var riderName = rider.text().toLowerCase();
        rider.append(getGoogleSearchLink("cycling " + riderName, true));
        rider.append(getGoogleSearchLink("twitter " + riderName, false, 
                                            chrome.extension.getURL('logo/twitter.png')));
        chrome.runtime.sendMessage({
            action: "queryRiderNote",
            id: riderid
        }, function (response) {
            parseRacePositions(riderid);

            $("#wrapper").prepend("<div id='extension-inject'></div>");
            $("#extension-inject").prepend(Handlebars.getTemplate("notes")(
                {
                    notes: response.notes,
                    scores: cachedScores[riderid].general
                }
            ));
            $("#extension-inject").append(Handlebars.getTemplate('noteform')());
            $("#extension-inject").append(Handlebars.getTemplate('noteformupdate')());
            dialogFormSetup("#dialog-new", "#send-new", "#cancel-new", "#note-area-new",
                function (note) {
                    chrome.runtime.sendMessage({
                        action: "queryNewNote",
                        rider: {
                            id: riderid,
                            note: note.note
                        }
                    }, function (response) {
                        refresh(response);
                    });
                });
            dialogFormSetup("#dialog-update", "#send-update", "#cancel-update", "#note-area-update",
                function (note) {
                    chrome.runtime.sendMessage({
                        action: "queryUpdateNote",
                        note: note
                    }, function (response) {
                        refresh(response);
                    });
                });

            attachListeners();
        });
    }

    if (isStartListPage(url)) {
        var race = $("#wrapper h1");
        var raceName = race.text().toLowerCase().replace("manager", "");
        race.append(getRaceNotepad(raceName));
        race.append(getGoogleSearchLink("cycling "+raceName, true));
        chrome.runtime.sendMessage({
            action: "queryAllRidersIds"
        }, function (response) {
            var ajaxScoreRequest;
            var ajaxSpamTimeout;
            $('#wrapper a').each(function () {
                var a = $(this);
                var href = a.attr('href');
                if (href.indexOf("team") > -1) {
                    //append google news team search to parent h2
                    var teamName = a.attr('title');
                    if (teamName) {
                        a.parent().append(getGoogleSearchLink(teamName + raceName, true));
                        a.parent().append(getGoogleSearchLink(teamName + " twitter", false,
                            chrome.extension.getURL('logo/twitter.png')));
                    }
                } else if (href.indexOf("rider.php") > -1) {
                    var aRiderid = a.attr('href').split("=")[1];
                    response.forEach(function (riderid) {
                        if (aRiderid === riderid) {
                            // mark that this rider has a note;
                            a.html(a.text() + " [x]");
                        }
                    });
                    //add detailed score tooltip for rider's current seasson
                    a.tooltip({
                        track: true,
                        classes: {
                            "ui-tooltip": "highlight"
                        },
                        open: function (event, ui) {
                            ui.tooltip.css("max-width", "200px");
                            var cachedScore = cachedScores[aRiderid];
                            if (cachedScore) {
                                console.log("Showing cached score detail tooltip for: " + aRiderid);
                                ui.tooltip.append(scoreDetailTpl({
                                    scoresDetail: cachedScore.detailed
                                }));
                            } else {
                                ajaxSpamTimeout = setTimeout(function () {
                                    ajaxScoreRequest = $.ajax({
                                        url: href,
                                        type: "GET",
                                        dataType: "html",
                                        success: function (data) {
                                            console.log("Loading and showing score detail tooltip for: " + aRiderid);
                                            parseRacePositions(aRiderid, data);
                                            ui.tooltip.append(scoreDetailTpl({
                                                scoresDetail: cachedScores[aRiderid].detailed
                                            }));
                                        },
                                        error: function (xhr, status) {
                                            console.log(status + ": " + xhr);
                                        },
                                        complete: function () {
                                            ajaxScoreRequest = null;
                                        }
                                    });
                                }, 2000);
                            }
                        },
                        close: function (event, ui) {
                            if (ajaxScoreRequest != null) {
                                ajaxScoreRequest.abort();
                                ajaxScoreRequest = null;
                                console.log("Abort prev request");
                            }
                            clearTimeout(ajaxSpamTimeout);
                        },
                    });

                }
            });//each func
        });//chrome msg func
    }

    if(isTeamPage(url)){
        var team = $("#wrapper h1");
        var teamName = team.text().toLowerCase();
        team.append(getGoogleSearchLink("cycling " + teamName, true));
        team.append(getGoogleSearchLink("twitter " + teamName, false, chrome.extension.getURL('logo/twitter.png')));
    }

    function getGoogleSearchLink(query, isNews, logo) {
        query = query.toLowerCase().split(' ').join('+');
        var newsq = isNews ? "&source=lnms&tbm=nws" : "";
        var href = ("https://www.google.com/search?q=" + query + newsq);
        var googleLogo = logo || chrome.extension.getURL('logo/google.png');
        return '<a href="' + href + '" target="_blank"> <img src="' + googleLogo + '"></a>';
    }

    function getRaceNotepad(raceName) {
        var span = $('<img src="' + chrome.extension.getURL('logo/note.png') + '"/>');
        var url = chrome.extension.getURL('template/racenotepad.html#' + raceid);
        span.click(function () {
            chrome.runtime.sendMessage({
                action: "queryRaceNotepadPop",
                race: {
                    id: raceid,
                    name: raceName
                }
            });
        });
        return span;
    }

    function parseRacePositions(rid, doc) {
        var scoreCalculator = new ScoreCalculator();
        var table = (doc) ? $(doc).find("table.sortTabell.tablesorter tbody") :
            $("table.sortTabell.tablesorter tbody");
        var rows = table.find("tr");
        rows.each(function () {
            var row = $(this);
            var colHash = row.find('td:eq(1)').text().trim();
            var colWhat = row.find('td:eq(2)').text().trim();
            var colWhere = row.find('td:eq(7)').text().trim();
            if (colHash !== "Track" &&
                colWhat !== "CX" &&
                colWhat !== "Stage race" &&
                colWhat !== "Points" &&
                colWhat !== "Grand Tour" &&
                colWhat !== "Mountains" &&
                colWhat !== "Youth" &&
                colWhat.indexOf("TTT") === -1 &&
                colWhere.indexOf("TTT") === -1 &&
                colHash.indexOf("CX") === -1
            ) {
                var raceType = row.find('td:eq(6)').find("img").attr("src") || "img/mini/Ukjent.gif"
                var raceClass = colHash;
                var position = row.find('td:eq(4)').text().trim();
                scoreCalculator.add(raceClass, raceType, position);
            }
        });
        cachedScores[rid] = {};
        cachedScores[rid].detailed = scoreCalculator.generateDetail();
        cachedScores[rid].general = scoreCalculator.generateGeneral();
    }

    function dialogFormSetup(dialogEl, submitEl, cancelEl, textEl, action) {
        var dialog = $(dialogEl);
        dialog.dialog({
            autoOpen: false,
            height: 550,
            width: 500,
            show: {
                effect: "blind",
                duration: 500
            },
            hide: {
                effect: "blind",
                duration: 500
            }
        });

        if (submitEl) {
            $(submitEl).on('click', function () {
                var note = $(textEl).val().trim();
                if (note !== "") {
                    action({
                        id: $(dialogEl + " form").attr("id"),
                        note: note
                    });
                    dialog.dialog('close');
                } else {
                    alert("Please enter a note first");
                }
            });
        }

        if (cancelEl) {
            $(cancelEl).on('click', function () {
                dialog.dialog('close');
            });
        }
    }

    function refresh(response) {
        $("#notes").replaceWith(Handlebars.getTemplate("notes")({
            notes: response.notes,
            scores: cachedScores[riderid].general
        }
        ));
        attachListeners();
    }

    function attachListeners() {
        $('#noteslist a').each(function (i) {
            var a = $(this);
            var id = a.attr('id').split('$');
            var noteId = id[0];
            var action = id[1];
            if (action === 'delete') {
                a.click(function () {
                    chrome.runtime.sendMessage({
                        action: "queryDeleteNote",
                        noteId: noteId
                    }, function (response) {
                        refresh(response);
                    });
                });
            } else {
                a.click(function () {
                    //fill the text area with the note text
                    var divNote = a.parent().next();
                    var text = divNote.text();
                    $('#note-area-update').val(divNote.text().trim());
                    //we're attaching the note id to form
                    $("#dialog-update form").attr("id", noteId);
                    $("#dialog-update").dialog("open");
                });
            }
        });
        $("#opener").click(function () {
            $('#note-area-new').val("");
            $("#dialog-new").dialog("open");
        });
        $("#notes").tooltip({
            items: "img",
            content: scoreDetailTpl({ scoresDetail: cachedScores[riderid].detailed }),
            classes: {
                "ui-tooltip": "highlight"
            }, open: function (event, ui) {
                ui.tooltip.css("max-width", "200px");
            }
        });

    }

    function getRiderId(url) {
        var regex = /https:\/\/firstcycling\.com\/rider\.php\?r=([0-9]+).*/g;
        var match, riderId = -1;
        while ((match = regex.exec(url)) !== null) {
            if (match.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            match.forEach(function (m, groupIndex) {
                // 0 index is the whole match
                // we are only interested in the capture group that gives us the rider id
                if (groupIndex == 1) {
                    riderId = m;
                }
            });
        }
        return riderId;
    }

    function getRaceId(url) {
        function regexRaceId(url) {
            var regex = /https:\/\/firstcycling\.com\/race\.php\?r=([0-9]+).*/g;
            var match, raceId = -1;
            while ((match = regex.exec(url)) !== null) {
                if (match.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                match.forEach(function (m, groupIndex) {
                    // 0 index is the whole match
                    // we are only interested in the capture group that gives us the race id
                    if (groupIndex == 1) {
                        raceId = m;
                    }
                });
            }
            return raceId;
        }
        var raceId = regexRaceId(url);
        if (raceId === -1) {
            //if we got no id, then we search for link of "startlist" and then we parse their url;
            var aUrl = $('.sidemeny a:eq(1)');
            if (aUrl) {
                var href = "https://firstcycling.com/" + aUrl.attr('href');
                raceId = regexRaceId(href);
            }
        }
        return raceId;
    }

    function isStartListPage(url) {
        return url.indexOf("k=start") > -1 || url.indexOf("manadm.php?konk") > -1;
    }

    function isTeamPage(url){
        return url.indexOf("team.php?l=") > -1;
    }

    function ScoreCalculator() {

        var points = [100, 80, 60, 50, 45, 40, 36, 32, 29, 26, 24, 22, 20, 18, 16];
        var scores = {};
        var scoresDetail = {};

        function result(position) {
            if (position === "DNS" || position === "DNF" || position > 30)
                return 0;
            var score = 0;
            if (position < 16) {
                score = points[position - 1];
            } else {
                score = 31 - position;
            }
            return score;
        }

        function addGeneralScore(raceType, position) {
            scores[raceType] = scores[raceType] || {
                score: 0,
                raceCount: 0
            };
            scores[raceType].raceCount += 1;
            scores[raceType].score += result(position);
        }

        function addDetailScore(raceClass, raceType, position) {
            scoresDetail[raceClass] = scoresDetail[raceClass] || {};
            scoresDetail[raceClass][raceType] = scoresDetail[raceClass][raceType] || {
                score: 0,
                raceCount: 0
            }
            scoresDetail[raceClass][raceType].raceCount += 1;
            scoresDetail[raceClass][raceType].score += result(position);
        }

        //api
        this.add = function (raceClass, raceType, position) {
            addGeneralScore(raceType, position);
            addDetailScore(raceClass, raceType, position);
        };

        this.generateGeneral = function () {
            return transform(scores);
        };

        this.generateDetail = function () {
            var out = {};
            Object.keys(scoresDetail).forEach(function (key) {
                out[key] = transform(scoresDetail[key]);
            });
            return out;
        };

        function transform(obj) {
            return Object.keys(obj).map(function (key) {
                return {
                    raceType: key,
                    score: Math.floor(obj[key].score / obj[key].raceCount)
                };
            }).sort(function (curr, next) {
                return next.score - curr.score;
            });
        }

    }
}

