var isConnected = false;
var cachedRider = {};
var cachedRaceNotes = [];

//config, email, password are from firebase-auth.js
firebase.initializeApp(config);
tryConnect();

function tryConnect() {
    firebase.auth()
        .signInWithEmailAndPassword(email, password)
        .catch(function (error) {
            isConnected = false;
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode + " : " + errorMessage);
            showError(errorMessage);
            return Promise.reject(error);
        })
        .then(function (result, error) {
            isConnected = true;
        });
}

function dateFormat(millis) {
    return new Date(millis).toLocaleDateString();
}

function handleQueryRiderNote(rider) {
    return new Promise(function (resolve, reject) {

        function convert(val) {
            var notes = [];
            if (val !== null) {
                //convert the object into an array of [{date:... , note:...}]
                Object.keys(val).forEach(function (key) {
                    notes.push({
                        id: key,
                        date: dateFormat(val[key].date),
                        note: val[key].note
                    });
                });
            }
            return notes;
        }

        function getNotes(riderId) {
            if (cachedRider.id && cachedRider.id == riderId) {
                console.log("Loading from memory cache. Rider id: " + riderId);
                resolve(cachedRider)
            } else {
                console.log("Loading from remote db. Rider id: " + riderId);
                firebase.database()
                    .ref("notes/" + rider.id)
                    .once("value", function (result) {
                        cachedRider.notes = convert(result.val());
                        cachedRider.id = rider.id;
                        resolve(cachedRider);
                    });
            }
        }

        getNotes(rider.id);

    });
}

function handleQueryNewNote(rider) {
    return new Promise(function (resolve, reject) {
        var now = Date.now();
        var dto = { date: now, note: rider.note };
        firebase.database().ref("riders/" + rider.id).set(true);
        firebase.database().ref("notes/" + rider.id + "/")
            .push(dto)
            .then(function (saved) {
                dto.id = saved.key;
                dto.date = dateFormat(now);
                cachedRider.notes.push(dto);
                resolve(cachedRider);
            });
    });
}

function handleQueryUpdateNote(updateNote) {
    return new Promise(function (resolve, reject) {
        var now = Date.now();
        var dto = { date: now, note: updateNote.note };
        firebase.database().ref("notes/" + cachedRider.id + "/" + updateNote.id)
            .update(dto)
            .then(function () {
                cachedRider.notes.forEach(function (note) {
                    if (note.id === updateNote.id) {
                        note.date = dateFormat(dto.date);
                        note.note = dto.note;
                    }
                });
                resolve(cachedRider);
            });
    });
}

function handleQueryDeleteNote(noteId) {
    return new Promise(function (resolve, reject) {
        //if we are about to delete the last note entry, remove rider from riders
        if (cachedRider.notes.length === 1) {
            firebase.database()
                .ref("riders/" + cachedRider.id).remove();
        }
        firebase.database()
            .ref("notes/" + cachedRider.id + "/")
            .child(noteId)
            .remove()
            .then(function () {
                //delete it from cache too
                for (var i = cachedRider.notes.length - 1; i >= 0; i--) {
                    if (cachedRider.notes[i].id === noteId) {
                        cachedRider.notes.splice(i, 1);
                        break;
                    }
                }
                resolve(cachedRider);
            });
    });
}

function handleQueryAllRidersIds() {
    function convert(val) {
        var riderIds = [];
        if (val !== null) {
            Object.keys(val).forEach(function (key) {
                riderIds.push(key);
            });
        }
        return riderIds;
    }

    return new Promise(function (resolve, reject) {
        function fetchAllRidersIds() {
            firebase.database()
                .ref("riders")
                .once("value", function (snapshot) {
                    resolve(convert(snapshot.val()));
                });
        }
        fetchAllRidersIds();
    });
}

function handleQueryRaceNotepadPop(race) {
    var url = chrome.extension.getURL('template/racenotepad.html#' + race.id + "#" + race.name);
    chrome.windows.create({ 'url': url, 'type': 'popup', height: 600, width: 525} );
}

function handleQueryRaceNotepadSave(race) {
    var races = firebase.database().ref("races/" + race.id);
    var firebaseRace = {
        name: race.name,
        content: race.content
    }
    if (!race.content) {
        races.remove().then(function () {
            deleteCachedRace(race.id);
        });
    } else {
        races.set(firebaseRace).then(function () {
            upsertCachedRace(race);
        });
    }
}

function getCachedRaceById(raceId) {
    var result = cachedRaceNotes.filter(function (el) {
        return el.id === raceId;
    });
    if (result)
        return result[0];
}

function upsertCachedRace(race) {
    var cachedRace = getCachedRaceById(race.id);
    if (cachedRace) {
        cachedRace.id = race.id;
        cachedRace.content = race.content;
        cachedRace.name = race.name
    } else {
        cachedRaceNotes.push(race);
    }
}

function deleteCachedRace(raceid) {
    for (var i = cachedRaceNotes.length - 1; i >= 0; i--) {
        if (cachedRaceNotes[i].id === raceid) {
            cachedRaceNotes.splice(i, 1);
            break;
        }
    }
}

function handleQueryRaceNotepadLoad(race) {
    return new Promise(function (resolve, reject) {
        var cachedRace = getCachedRaceById(race.id);
        if (cachedRace) {
            resolve(cachedRace);
        } else {
            firebase.database()
                .ref("races/" + race.id)
                .once("value", function (result) {
                    var _val = result.val();
                    var newCachedRace = {
                        id: race.id,
                        content: _val ? _val.content : "",
                        name: _val ? _val.name : race.name
                    }
                    upsertCachedRace(newCachedRace);
                    resolve(newCachedRace);
                });
        }
    });
}

function showError(error) {
    chrome.runtime.sendMessage({
        type: "error",
        message: error
    });
    alert(error);
}

function ready() {
    chrome.runtime.sendMessage({ type: "ready" });
}

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        var promise;
        if (request.action == "checkForReady") {
            promise = Promise.resolve(isConnected);
        } if (request.action === "queryRiderNote") {
            promise = handleQueryRiderNote(request);
        } else if (request.action === "queryNewNote") {
            promise = handleQueryNewNote(request.rider);
        } else if (request.action === "queryDeleteNote") {
            promise = handleQueryDeleteNote(request.noteId);
        } else if (request.action === "queryUpdateNote") {
            promise = handleQueryUpdateNote(request.note);
        } else if (request.action === "queryAllRidersIds") {
            promise = handleQueryAllRidersIds();
        } else if (request.action === "queryRaceNotepadPop") {
            handleQueryRaceNotepadPop(request.race);
        } else if (request.action === "queryRaceNotepadSave") {
            handleQueryRaceNotepadSave(request.race);
        } else if (request.action === "queryRaceNotepadLoad") {
            promise = handleQueryRaceNotepadLoad(request.race);
        }
        if (promise)
            promise
                .catch(function (error) {
                    showError(error)
                    return Promise.reject(error);
                })
                .then(function (result, error) {
                    sendResponse(result);
                });
        return true; // asssure this is async
    });

