document.addEventListener('DOMContentLoaded', ready);

var textArea;

function ready() {
    textArea = document.getElementById("note-race");
    document.getElementById("send-new").addEventListener('click', saveNote);
    chrome.runtime.sendMessage({
        action: "queryRaceNotepadLoad",
        race: getRace()
    }, function (response) {
        console.log(response);
        if (response) {
            textArea.value = response.content;
            document.getElementById("race-name").innerHTML = response.name;
        }
    });
}

function saveNote() {
    var raceData = getRace(); 
    chrome.runtime.sendMessage({
        action: "queryRaceNotepadSave",
        race: {
            id: raceData.id,
            name: raceData.name,
            content: textArea.value
        }
    });
}

function getRace() {
    var segments = window.document.URL.split("#");
    return {
        id: segments[1],
        name: segments[2]
    };
}


